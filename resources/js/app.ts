import Vue from 'vue';
import Board from './components/Board.vue';


const app = new Vue({
	el: '#app',
	components: {
		'board': Board,
	},
});
